package main

import (
	"net/http"
)

func main() {
	http.HandleFunc("/", renderIndexPage)
	http.ListenAndServe(":8000", nil)
}

type PageViewModel struct {
	Title string
}