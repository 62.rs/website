package main

import (
	"html/template"
	"net/http"
	"os"
)

var indexTemplate *template.Template

func init() {
	indexTemplate = getIndexTemplate()
}

func getIndexTemplate() *template.Template {
	t, err := template.ParseFiles("index.html")
	if err != nil {
		os.Exit(1)
	}
	t = template.Must(t, err)
	return t
}

func renderIndexPage(w http.ResponseWriter, r *http.Request) {
	indexVM := PageViewModel{
		Title:"62.rs: Učimo programiranje! :)",
	}
	indexTemplate.Execute(w, indexVM)
}

